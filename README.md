# flip-matrix-bot Documentation

Copyright © 2023 Andrew "HER0" Conrad

Documentation for [flip-matrix-bot](https://gitlab.com/FriendlyLinuxPlayers/bots/flip-matrix-bot). Available online at the following URL: <https://friendlylinuxplayers.gitlab.io/bots/matrix-bot-docs/>

This documentation is freely available on
[GitLab](https://gitlab.com/FriendlyLinuxPlayers/bots/matrix-bot-docs) under the terms of
the GNU FDL version 1.3. See `LICENSE.md` for details.

By participating in this community, you agree to abide by the terms of our
[Code of Conduct](https://FriendlyLinuxPlayers.org/conduct).
