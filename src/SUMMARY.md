# Summary

[Introduction](intro.md)

# Interacting with the bot

- [Usage overview](end-user/overview.md)
- [The events command](end-user/events/events.md)
    - [Listing events](end-user/events/list.md)
    - [Adding events](end-user/events/add.md)
    - [Modify events](end-user/events/modify.md)
    - [Cancel events](end-user/events/cancel.md)
- [Information command](end-user/information.md)

# Running the bot

- [Installation](admin/install.md)
- [Matrix prerequisites](admin/matrix.md)
- [Starting the bot](admin/run.md)

# Contributing

- [Contributing to the bot]()
- [Contributing to the documentation]()
