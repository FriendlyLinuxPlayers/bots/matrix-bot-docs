# Installation

## Requirements

- A Rust toolchain (as installed with [rustup](https://rustup.rs/))
- OpenSSL (except on Windows and macOS)

### Operating system

Development, testing, and production use take place on x86-64 Linux machines. However, `flip-matrix-bot` should work on [any platform supported by Rust](https://doc.rust-lang.org/stable/rustc/platform-support.html).

### Minimum Rust version

`flip-matrix-bot` is built against the latest stable version of Rust, and sometimes uses new features in these versions. Older versions are untested, and may not work.

## Build instructions

First clone the repository:

```
https://gitlab.com/FriendlyLinuxPlayers/bots/flip-matrix-bot.git
```

Then compile and install the bot:

```
cargo install --path flip-matrix-bot --locked
```
