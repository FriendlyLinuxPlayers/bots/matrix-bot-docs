# Matrix prerequisites

The bot requires a preexisting Matrix account that has membership in (or is able to join) two rooms:

- The room that the bot is used in. This is the only place the bot will listen for events or send messages.
- The room that the bot uses to store all of its data. For the best performance, this should be a room that is used exclusively for this purpose, with a minimal amount of other Matrix events sent in that room.

## Initial setup recommendations

You can create a Matrix account and join the appropriate rooms using a standard [Matrix client](https://matrix.org/ecosystem/clients/), such as Element. Typically, the following steps can be followed:

1. Register a new account for the bot on a Matrix server.
1. Join the room that the bot will be used in.
1. Create a dedicated room for the bot to store data.
1. Optionally, invite admin accounts to the newly-created data store room. In Element, the stored data can be inspected by send a message that reads `/devtools`, then enabling the **Show hidden events in the timeline** setting. When doing this, it is important that these other accounts do not send other events in the room. Don't use the data store room for discussion.
