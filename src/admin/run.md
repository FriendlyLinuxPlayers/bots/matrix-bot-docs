# Starting the bot

You can customize the configuration of `flip-matrix-bot` with different options specified when running the command.

## Usage

`flip-matrix-bot [OPTIONS] --username <USERNAME> --password <PASSWORD> --room <ROOM> --storage-room <STORAGE_ROOM>`

## Options

| Short option | Long option       | Parameter         | Description |
|--------------|-------------------|-------------------|-------------|
| `-H`         | `--homeserver`    | `<HOMESERVER>`    | URL of bot's homeserver [default: https://matrix.flip.earth] |
| `-u`         | `--username`      | `<USERNAME>`      | Bot's username (e.g. for "@botty:flip.earth" it would be "botty") |
| `-p`         | `--password`      | `<PASSWORD>`      | Bot's password |
| `-r`         | `--room`          | `<ROOM>`          | Room alias or ID to join (e.g. "#room:flip.earth") |
| `-s`         | `--storage-room`  | `<STORAGE_ROOM>`  | Room ID for storing data (e.g. "!<ID>:flip.earth") |
| `-d`         | `--display-name`  | `<DISPLAY_NAME>`  | Display name for the bot to use |
| `-c`         | `--calendar-path` | `<CALENDAR_PATH>` | Where to write the iCalendar file [default: ./events.ics] |
| `-C`         | `--calendar-url`  | `<CALENDAR_URL>`  | URL where the iCalendar file can be accessed (e.g. "webcal://flip.earth/feed/events.ics") |
| `-w`         | `--website-path`  | `<WEBSITE_PATH>`  | Directory to write event data for the website (e.g. "/var/www/flip-grav/user/pages/02.events/flip-matrix-bot") [default: ./] |
| `-W`         | `--website-url`   | `<WEBSITE_URL>`   | Where the event pages on the website are accessible (e.g. "https://friendlylinuxplayers.org/events/") |
| `-h`         | `--help`          |                   | Print help |
| `-V`         | `--version`       |                   | Print version |

## Examples

### Minimum arguments

```
flip-matrix-bot -h https://matrix.your.homeserver -u username -p hunter2 -r #example-room:your.homeserver -s !abcdefghijklmnop:your.homeserver
```
