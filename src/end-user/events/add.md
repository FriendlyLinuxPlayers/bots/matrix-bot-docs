# Adding events

Use the `events` command with the `add` subcommand to schedule a new event.

## Usage

`!f events add [OPTIONS] <GAME> <GAME_URL> <TIME> [TIMEZONE]`

## Positional arguments

| Argument     | Default value  | Description           |
|--------------|----------------|-----------------------|
| `<GAME>`     | N/A (required) | The game to be played |
| `<GAME_URL>` | N/A (required) | Link to the game (for example, the Steam store page) |
| `<TIME>`     | N/A (required) | Time that the event starts (e.g. "2017-06-30 15:40" or "Friday 6pm") |
| `[TIMEZONE]` | UTC            | [Timezone](https://docs.rs/chrono-tz/latest/chrono_tz/enum.Tz.html#variants) of the scheduled time (e.g. "US/Central", "CET", or "Etc/GMT+4") |

## Options

| Short option | Long option                | Parameter           | Description |
|--------------|----------------------------|---------------------|-------------|
| `-n`         | `--name`                   | `<NAME>`            | The title of this event (e.g. "Virtual board game night") |
| `-m`         | `--meeting-place`          | `<MEETING_PLACE>`   | Where everyone can talk during the event (like a Mumble channel or Matrix room) [default: the Event channel on `mumble.flip.earth`] |
| `-O`         | `--ownership-not-required` |                     | Ownership of the game is not required to attend this event |
| `-s`         | `--server`                 | `<SERVER>`          | The server that the game will be played on |
| `-p`         | `--server-password`        | `<SERVER_PASSWORD>` | The password required to connect to the game server |

## Examples

### By absolute time in UTC

Command:

```
!f events add "Risk of Rain 2" https://store.steampowered.com/app/632360/Risk_of_Rain_2/ "2023-07-27 23:00"
```

### By relative time in Brasília Time

Command:

```
!f events add OpenRCT2 https://openrct2.org/ "Saturday 1pm" America/Sao_Paulo
```

### Playing through a stream

Some events may (optionally or otherwise) use streaming (like [Steam Remote Play Together](https://store.steampowered.com/remoteplay/) or [Parsec](https://parsec.app/)) to allow participants to join without running the game locally. This can be indicated in the event name and by setting the "ownership not required" flag.

Command:

```
!f events add --name "Jackbox 7 (with Steam Remote Play)" --ownership-not-required "The Jackbox Party Pack 7" https://store.steampowered.com/app/1211630/The_Jackbox_Party_Pack_7/ "2023-10-06 14:00"
```
