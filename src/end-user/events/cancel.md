# Cancel events

Use the `events` command with the `cancel` subcommand to cancel an upcoming event. This is limited to event hosts who are cancelling events they have scheduled, or room moderators.

## Usage

`!f events cancel <EVENT_ID>`

### Alias

Alternately, the alias `can` may be used:

`!f events can <EVENT_ID>`

## Event ID

The event to be cancelled can be identified either through a positional argument or an option.

| Argument or parameter | Short option | Long option | Description |
|-----------------------|--------------|-------------|-------------|
| `<ID>`                |              |             | Cancel by event ID, as shown when listing events |
| `<MX_ID>`             | `-m`         | `--mx-id`   | Cancel by the Matrix event ID of the command which created the event |

## Examples

### By ID

Command:

```
!f events cancel eaeff0d183a6e910
```
