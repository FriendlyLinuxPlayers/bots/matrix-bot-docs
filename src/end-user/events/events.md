# The events command

The most important feature of `flip-matrix-bot` is the `events` command. With this, events can be scheduled by community members, and those events can be modified or listed. The following chapters will go into detail and provide examples of how the `events` command works.

## Usage

`!f events …`

### Alias

Alternately, the alias `e` may be used:

`!f e …`

## Commands

- [`add`](add.md): Schedule a new event
- [`modify`](modify.md): Modify an upcoming event
- [`cancel`](cancel.md): Cancel an upcoming event
