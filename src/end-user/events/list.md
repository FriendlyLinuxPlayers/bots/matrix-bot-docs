# Listing events

Use the `events` command without a subcommand to list upcoming events. Optionally, get information about events that match a given ID.

## Usage

`!f events [OPTIONS]`

### Alias

Alternately, the alias `e` may be used:

`!f e [OPTIONS]`

## Options

| Short option | Long option | Parameter | Description                   |
|--------------|-------------|-----------|-------------------------------|
| `-l`         | `--long`    |           | Display events in long format |
| `-i`         | `--id`      | `<ID>`    | Display events matching an ID |
| `-m`         | `--mx-id`   | `<MX_ID>` | Display event created by the message with a given Matrix event ID |

## Examples

### List all upcoming events

Command:

```
!f events
```

Bot response:

> Upcoming events:
>
> - `40054` - [**Payday 2 (with Proton)**](https://friendlylinuxplayers.org/events/400549eed0856579) - in 2 days from now  
>     Game: [Payday 2](https://store.steampowered.com/app/218620/PAYDAY_2/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sun, Jul 16 7:00pm UTC+00:00.  
>
> - `2423f` - [**Silica**](https://friendlylinuxplayers.org/events/2423f7c280f6a844) - in a week from now  
>     Game: [Silica](https://store.steampowered.com/app/1494420/Silica/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sat, Jul 22 7:00pm UTC+00:00.  
>
> - `73732` - [**Trine 2**](https://friendlylinuxplayers.org/events/737326f499c9ff1f) - in 2 weeks from now  
>     Game: [Trine 2](https://store.steampowered.com/app/35720/Trine_2_Complete_Story/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sun, Jul 30 9:00pm UTC+00:00.  
>
> - `5cd63` - [**Star Wars Battlefront II (2017)**](https://friendlylinuxplayers.org/events/5cd6382852d19af7) - in 3 weeks from now  
>     Game: [Star Wars Battlefront II (2017)](https://store.steampowered.com/app/1237950/STAR_WARS_Battlefront_II/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sat, Aug 5 8:00pm UTC+00:00.  
>
> - `b19c6` - [**Heroes of Hammerwatch**](https://friendlylinuxplayers.org/events/b19c65a8c872e8dc) - in a month from now  
>     Game: [Heroes of Hammerwatch](https://store.steampowered.com/app/677120/Heroes_of_Hammerwatch/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sun, Aug 13 6:00pm UTC+00:00.  
>
> - `52506` - [**Arma Reforger**](https://friendlylinuxplayers.org/events/525066ea0e8bdf15) - in a month from now  
>     Game: [Arma Reforger](https://store.steampowered.com/app/1874880/Arma_Reforger/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sat, Aug 19 7:00pm UTC+00:00.  
>
> - `11679` - [**Barotrauma**](https://friendlylinuxplayers.org/events/116799628f2805e2) - in 2 months from now  
>     Game: [Barotrauma](https://store.steampowered.com/app/602960/Barotrauma/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sat, Sep 2 7:00pm UTC+00:00.  
>
> - `e0b83` - [**PULSAR: Lost Colony**](https://friendlylinuxplayers.org/events/e0b83e82866d204) - in 2 months from now  
>     Game: [PULSAR: Lost Colony](https://store.steampowered.com/app/252870/PULSAR_Lost_Colony/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on Sat, Sep 16 6:00pm UTC+00:00.  
>
>
>
> Subscribe to the calendar feed: [webcal://flip.earth/feed/events.ics](webcal://flip.earth/feed/events.ics)

### Long format of an event by ID

Command:

```
!f events --long --id b19c6
```

Bot response:

> Event found:
>
> - `b19c65a8c872e8dc` - [**Heroes of Hammerwatch**](https://friendlylinuxplayers.org/events/b19c65a8c872e8dc) - in a month from now  
>     Game: [Heroes of Hammerwatch](https://store.steampowered.com/app/677120/Heroes_of_Hammerwatch/) - Hosted by [HER0](https://matrix.to/#/@HER0:matrix.org) on 2023-08-05T20:00:00+00:00 in the Event channel on `mumble.flip.earth`.  
>     Scheduled in [Friendly Linux Players](https://matrix.to/#/!rnMfpLpDyrSTuFwqSj:flip.earth?via=flip.earth&via=matrix.org&via=catgirl.cloud) on [2023-06-13 21:10:22.222 UTC](https://matrix.to/#/!rnMfpLpDyrSTuFwqSj:flip.earth/$16866906224937gftSi:matrix.org?via=flip.earth&via=matrix.org&via=catgirl.cloud).
