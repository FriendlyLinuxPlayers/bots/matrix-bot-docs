# Modify events

Use the `events` command with the `modify` subcommand to modify an exising event. This is limited to event hosts modifying upcoming events that they have scheduled, or room moderators modifying any event.

## Usage

`!f events modify <EVENT_ID> <OPTIONS>`

### Alias

Alternately, the alias `mod` may be used:

`!f events mod <EVENT_ID> <OPTIONS>`

## Event ID

The event to be modified can be identified either through a positional argument or an option.

| Argument or parameter | Short option | Long option | Description |
|-----------------------|--------------|-------------|-------------|
| `<ID>`                |              |             | Modify by event ID, as shown when listing events |
| `<MX_ID>`             | `-m`         | `--mx-id`   | Modify by the Matrix event ID of the command which created the event |

## Options

At least one modification option must be specified.

| Short option | Long option                | Parameter           | Description |
|--------------|----------------------------|---------------------|-------------|
| `-n`         | `--name`                   | `<NAME>`            | Set the title of the event |
| `-g`         | `--game`                   | `<GAME>`            | Set the game to be played |
| `-u`         | `--game-url`               | `<GAME_URL>`        | Set the game URL |
| `-t`         | `--time`                   | `<TIME>`            | Set the event start time (e.g. "2017-06-30 15:40" or "Friday 6pm") |
| `-T`         | `--timezone`               | `<TIMEZONE>`        | Set the [timezone](https://docs.rs/chrono-tz/latest/chrono_tz/enum.Tz.html#variants) for the event start time (e.g. "US/Central", "CET", or "Etc/GMT+4")[^note] |
| `-M`         | `--meeting-place`          | `<MEETING_PLACE>`   | Set where everyone can talk during the event |
| `-o`         | `--ownership-required`     |                     | Ownership of the game is required to attend this event |
| `-O`         | `--ownership-not-required` |                     | Ownership of the game is not required to attend this event |
| `-s`         | `--server`                 | `<SERVER>`          | The server that the game will be played on |
| `-p`         | `--server-password`        | `<SERVER_PASSWORD>` | The password required to connect to the game server |
| `-C`         | `--uncancel`               |                     | Revert the cancelling of a [cancelled event](cancel.md) |

[^note]: Changing the timezone to one with a different offset without changing the time will change the start time of the event.

## Examples

### Editing the event name

Command:

```
!f events modify 400549eed0856579 --name "Payday 2 (with Proton)"
```

### Correcting the timezone

If an event is scheduled without a timezone specified, UTC is assumed. If you forgot to specify a timezone (or set it incorrectly), and so the start time is not correct, modifying the event with a timezone will adjust the start time to the given timezone.

For an event that was scheduled without a timezone (and so was set to UTC), the following message would change the start time to be in Eastern European Time. The event would take place two or three hours earlier, depending on whether summer time (daylight savings) is in effect at the time that the event takes place.

Command:

```
!f events modify d544478dd9dfde2d --timezone EET
```

### Revert cancellation

Command:

```
!f events modify 92749fa8abccaa18 -C
```
