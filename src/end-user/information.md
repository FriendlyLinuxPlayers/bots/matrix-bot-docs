# Information command

The `information` command has a number of subcommands for displaying useful information.

## Usage

`!f information [COMMAND]`

### Aliases

Alternately, the alias `info` may be used:

`!f info [COMMAND]`

Or the alias `i`:


`!f i [COMMAND]`

## Commands

| Command       | Alias      | Descripion |
|---------------|------------|------------|
| `website`     |            | Get a link to the website |
| `conduct`     |            | Get a link to the Code of Conduct |
| `space`       |            | Get information on our Matrix space |
| `events`      | `calendar` | Get information on community events |
| `mumble`      | `voice`    | Get information about our Mumble server |
| `steam`       |            | Get information about our Steam group |
| `source`      |            | Get a link to the flip-matrix-bot source code |
| `gitlab`      |            | Get a link to our GitLab organization |
| `bot-threads` |            | Get information on using threads with the bot |
| `guests`      |            | Get information on guests in the community |

## Examples

### Bot source code link

Command:

```
!f information source
```
