# Usage overview

In a Matrix room where flip-matrix-bot is running, you can interact with the bot by sending a message starting with `!f`. This works similarly to a typical CLI application, with commands and parameters used to perform various operations. The following chapters will go into detail and provide examples of how this works.

The bot only responds to new messages sent in the room it is operating in. Edits to previous messages and direct messages are ignored.

The bot can also send messages which aren't in direct reply to another message. This includes greeting new room members and reminding of upcoming events.

## Usage

`!f <COMMAND>`

## Commands

There are multiple commands which can be specified to trigger different functionality:

- [`events`](events/events.md): Schedule, list, and manage community gaming events
- [`information`](information.md): Display information on the FLiP community

### Aliases

Commands and their subcommands may have aliases, which are shorthand for writing the full command. For example, the following messages contain the same commands.

Using standard command names:

`!f events modify …`

Using command aliases:

`!f e mod …`

## Help output

Within every command or subcommand, get details on the available commands and options with the `help` subcommand or `--help` option.

### General behavior

Help output and replies to messages with invalid input will be sent in a [thread](https://spec.matrix.org/latest/client-server-api/#threading). You may, optionally, reply within the thread when forming your next messages for the bot. Doing so will reduce clutter in the main timeline of the room if mistakes are made, or if you need more help output.

### Examples

The following messages will both result in the bot replying in a thread with the help output for modifying events.

Using the `--help` option:

```
!f events modify --help
```

Using the `help` command:

```
!f events help modify
```

## Quoting

Similar to the typical CLI application, each part of the message that is interpreted by the bot is separated by a space character. In order to provide arguments or parameters which contain a space in them, surround the argument or parameter in quotes.

For example, to schedule an event for a game called `Neon Prime`, you must surround the game name in quotes:

```
!f events add "Neon Prime" …
```
