# Introduction

flip-matrix-bot is a [Matrix](https://matrix.org) bot for the [Friendly Linux Players](https://friendlylinuxplayers.org) community.

One of the primary goals is to provide a CLI-like interface for anyone in the community to schedule gaming events. Join [#flip:flip.earth](https://matrix.to/#/#flip:flip.earth) and send a message starting with `!f` to get started with using it. Alternately, you can see it being tested in [#bots:flip.earth](https://matrix.to/#/#bots:flip.earth). Discussion on the bot itself belongs in either [the issues](https://gitlab.com/FriendlyLinuxPlayers/bots/flip-matrix-bot/-/issues) or [#flip-matrix-bot:flip.earth](https://matrix.to/#/#flip-matrix-bot:flip.earth).

The bot's source code is freely available on
[GitLab](https://gitlab.com/bots/FriendlyLinuxPlayers/flip-matrix-bot) under the terms of
the GNU GPL version 3. By participating in the Friendly Linux Players community, you agree to abide by the terms of our
[Code of Conduct](https://FriendlyLinuxPlayers.org/conduct).
